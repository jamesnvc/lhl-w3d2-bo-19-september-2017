//
//  OtherViewController.h
//  SeguesDemo
//
//  Created by James Cash on 19-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface OtherViewController : ViewController

@property (nonatomic,strong) NSString *thingToShow;

@end
