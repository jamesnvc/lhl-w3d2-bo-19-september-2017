//
//  ViewController.m
//  SeguesDemo
//
//  Created by James Cash on 19-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "OtherViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UITextField *thingTextField;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

// Old way
- (IBAction)goToOther:(id)sender {
    // We'd like to configure the other view controller in storyboard
    // if we make it this way, those outlets won't be set up, so we'd need to use a more complicated method
    // also, by doing it this way, we need to read the code to figure out what the flow of the app is like
    // whereas with segues, we can just look at the storyboard
    OtherViewController *ovc = [[OtherViewController alloc] init];
    //    ovc.thingLabel.text = @"whatever";
    [self presentViewController:ovc animated:YES completion:^{  }];
}

// This method is optional and will be default always be YES
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // identifier is optional, just for identifing which segue is happening right now
    // if you only have the one for this view controller, you can not bother
    if ([segue.identifier isEqualToString:@"GoToOther"]) {
        // want to pass data to the vc we're about to go to
        OtherViewController *ovc = segue.destinationViewController;
        // how do we get data to it?
        // the below wouldn't work (even if the label were public) because at this point
        // while the other view controller has been created, it's view isn't yet loaded
        // so all the subviews, including that label, are nil
        // soooo, calling the setter does nothing
        //        ovc.thingLabel.text = self.thingTextField.text;
        // we've added the property to the other view controller, so we can use that
        ovc.thingToShow = self.thingTextField.text;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
