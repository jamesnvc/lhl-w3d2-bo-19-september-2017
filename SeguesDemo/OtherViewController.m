//
//  OtherViewController.m
//  SeguesDemo
//
//  Created by James Cash on 19-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "OtherViewController.h"

@interface OtherViewController ()
@property (strong, nonatomic) IBOutlet UILabel *thingLabel;

@end

@implementation OtherViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor blueColor];

    self.thingLabel.text = self.thingToShow;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
